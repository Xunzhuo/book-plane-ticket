package javabean;
import java.sql.*;

public class db_conn {
	public Connection conn = null;
	public ResultSet res = null;
	public Statement st = null;
	public  db_conn() {
		String URL="jdbc:mysql://101.201.235.159:3306/jsp_plane_ticket_book?serverTimezone=UTC";
		String USER="root";
		String PWD="lxz991203";
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e){
			System.out.println(e);
		}try{
			conn = DriverManager.getConnection(URL,USER,PWD);
			st=conn.createStatement();
		}catch(SQLException e){
			System.out.println(e);
		}
	}
	public int executeInsert(String sql){
		int num=0;
		try{
			num=st.executeUpdate(sql);
		}
		catch(SQLException e){
			System.out.println("插入错误"+e.getMessage());
		}
		return num;
	}
	

	public ResultSet executeQuery(String sql){
		res=null;
		try{
			res=st.executeQuery(sql);
		}
		catch(SQLException e){
			System.out.print("查询错误"+e.getMessage());
		}
		return res;
	}
	

	public int Update(String sql){
		int num=0;
		try{
			num=st.executeUpdate(sql);
		}catch(SQLException ex){
			System.out.print("ִ更新错误"+ex.getMessage());
		}
		return num;
	}
	

	public int executeDelete(String sql){
		int num=0;
		try{
			num=st.executeUpdate(sql);
		}
		catch(SQLException e){
			System.out.print("ִ删除错误"+e.getMessage());
		}
		return num;
	}
	

	public void closeDB(){
		try{
			st.close();
			conn.close();
		}
		catch(Exception e){
			System.out.print("ִ关闭错误:"+e.getMessage());
		}
	}
}
